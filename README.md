# Syllabus

A collection of various lessons that I've written

1. [Installing and Running](#installing-and-running)
2. [The Lessons](#the-lessons)

___

## Installing and Running

> There are a few choices for viewing and running the Jupyter notebooks, and each have their pros and cons.

These are the two easiest options:

### Install Anaconda **_(recommended)_**

1. Open anaconda
2. Click on the 'Jupyter Notebook' tile
3. When the terminal launches, type `jupyter notebook` and hit enter.
4. An internet browser should open up displaying your home folder
5. Navigate to the plac where you saved the .ipynb files and click into it

* **_Pros_**: No internet required to run, can save changes locally
* **_Cons_**: Anaconda is massive to install, and is a little confusing to use

### Run from the github repository

> (**View/Read only!**)

This repository is hosted at at [https://github.com/tmck-code/Syllabus](https://github.com/tmck-code/Syllabus). You can navigate to one of the .ipynb files and use in github on the net, however this is read-only! You can't edit the file and save changes

### Install the latest python

You can find OSX python installers on the [official site](https://www.python.org/downloads/mac-osx/)

In particular, the installer for 64-bit Python 3.6.5 (latest stable at the time of writing) is here: [https://www.python.org/ftp/python/3.6.5/python-3.6.5-macosx10.9.pkg](https://www.python.org/ftp/python/3.6.5/python-3.6.5-macosx10.9.pkg)

1. Download and install in OSX
2. Open a terminal and check that it's installed `python3 -V`
3. Install Jupyter: `python3 -m pip install jupyter`

* **_Pros_**: no installation of anything required
* **_Cons_**: only on an internet connection, and you can't save your progress

___

## The Lessons

_This list will be updated as more lessons are written_

### Beginner

**Note**: These lessons do not start from ground 0, there are a few bits of prior knowledge that are assumed

1. Creating variables
2. Creating lists
3. Using a for loop
4. Using an if statement

Lessons will be added for these in the future.

1. Collections (`lists` & `dictionaries`)
2. OOP Basic Into (making a simple object)
